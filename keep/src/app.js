const Koa = require('koa')
  , app = new Koa()
  , server = require('http').createServer(app.callback())
  , io = require('socket.io')(server)
  , cors = require('koa-cors')
  , convert = require('koa-convert')
  , bodyparser = require('koa-bodyparser')
  , router = require('koa-router')()
  , datastore = require('nedb-promise')
  , bookStore = datastore({filename: '../books.json', autoload: true});

let booksLastUpdate = null;

app.use(async(ctx, next) => { //logger
  const start = new Date();
  await next();
  console.log(`${ctx.method} ${ctx.url} - ${new Date() - start}ms`);
});

app.use(async(ctx, next) => { //error handler
  try {
    await next();
  } catch (err) {
    setIssueRes(ctx.response, 500, [{error: err.message || 'Unexpected error'}]);
  }
});

app.use(bodyparser());
app.use(convert(cors()));

const BOOK = '/Book'
  , LAST_MODIFIED = 'Last-Modified'
  , ETAG = 'ETag'
  , OK = 200
  , CREATED = 201
  , NO_CONTENT = 204
  , NOT_MODIFIED = 304
  , BAD_REQUEST = 400
  , NOT_FOUND = 404
  , METHOD_NOT_ALLOWED = 405
  , CONFLICT = 409;

router
  .get(BOOK, async(ctx) => {
    let res = ctx.response;
    // let lastModified = ctx.request.get(LAST_MODIFIED);
    // if (lastModified && booksLastUpdate && booksLastUpdate <= new Date(lastModified).getTime()) {
    //   res.status = NOT_MODIFIED; //304 Not Modified (the client can use the cached data)
    // } else {
    res.body = await bookStore.find({});
    res.set({[LAST_MODIFIED]: new Date(booksLastUpdate)});
    //}
  })
  .post(BOOK, async(ctx) => {
    let book = ctx.request.body;
    let res = ctx.response;
    if (book.titlu && book.autori.length!==0 && book.editura && book.anAparitie && book.cuvCheie.length!==0) { //validation
      await createBook(res, book);
    } else {
        setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]); //400 Bad Request
    }
  });

const setIssueRes = (res, status, issue) => {
  res.body = {issue: issue};
  res.status = status; //Bad Request
}

const createBook = async(res, book) => {
  console.log(book);
  if(!book.titlu) return false;
  book.version = 1;
  book.updated = Date.now();
  let insertedBook = await bookStore.insert(book);
  booksLastUpdate = book.updated;
  delete insertedBook.updated;
  delete insertedBook.version;
  delete insertedBook._id;
  setBookRes(res, CREATED, insertedBook); //201 Created
  io.emit('book-created', insertedBook);
}

const setBookRes = (res, status, book) => {
  res.body = book;
  res.set({[ETAG]: book.version, [LAST_MODIFIED]: new Date(book.updated)});
  res.status = status; //200 Ok or 201 Created
    console.log(res);
}

app
  .use(router.routes())
  .use(router.allowedMethods());

io.on('connection', (socket) => {
  console.log('client connected');
  socket.on('disconnect', () => {
    console.log('client disconnected');
  })
});

(async() => {
  await bookStore.remove({});
  for(let i = 0; i < 10; i++) {
    await bookStore.insert({titlu: `Titlu ${i}`, autori: [`Autor ${i}1`,`Autor ${i}2`,`Autor ${i}3`], anAparritie:i*100, editura: `Editura${i}`,cuvCheie: [`CuvCheie ${i}1`,`CuvCheie ${i}2`,`CuvCheie ${i}3`], updated: Date.now()});
    console.log(`Book ${i} added`);
  }
})();

server.listen(3000);