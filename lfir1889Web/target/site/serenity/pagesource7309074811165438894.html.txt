<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Library</title>

</head>
<body>
    Titlu:<input name="titlu" style="" type="text"><br>
    Autori:<input name="autori" placeholder="Separati prin virgula" style="" type="text"><br>
    Editura:<input name="editura" style="" type="text"><br>
    An aparitie:<input name="anAparitie" style="" type="text"><br>
    Cuvinte cheie:<input name="cuvCheie" placeholder="Separate prin virgula" style="" type="text"><br>
    <button onclick="addBook()" name="addBookButton" style="">Adauga carte</button>


<script>
    function addBook() {
        var req = new XMLHttpRequest();
        var addBookButton = document.getElementsByName("addBookButton")[0];
        var notification = document.getElementsByName("message")[0];
        if(notification)
        {
            console.log("already existed");
        }
        else
        {
            notification = document.createElement("label");
            notification.setAttribute("name","message");
            document.getElementsByTagName("body")[0].appendChild(notification);
        }
        req.onreadystatechange = function() {
            if (this.readyState == 4 && (this.status == 200 || this.status == 201)) {
               console.log(this.responseText);
               notification.innerHTML = this.responseText;
            }
            else
                console.log(this.responseText);
                notification.innerHTML = this.responseText
        };
        req.open("POST","http://192.168.0.4:3000/Book",true);
        req.setRequestHeader('Content-Type','application/json');
        var titlu = document.getElementsByName("titlu")[0].value;
        var autori = document.getElementsByName("autori")[0].value;
        var editura = document.getElementsByName("editura")[0].value;
        var anAparitie = parseInt(document.getElementsByName("anAparitie")[0].value);
        var cuvCheie = document.getElementsByName("cuvCheie")[0].value;
        var book = {};
        book["titlu"] = titlu;
        book["editura"] = editura;
        book["anAparitie"] = anAparitie;
        book["autori"] = autori.split(',');
        book["cuvCheie"] = cuvCheie.split(',');
        req.send(JSON.stringify(book));
    }
</script>
<label name="message">{"titlu":"Titlu","editura":"Edit","anAparitie":123,"autori":["AA","BB"],"cuvCheie":["CC","DD"]}</label></body></html>