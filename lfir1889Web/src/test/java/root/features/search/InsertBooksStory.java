package root.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import root.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class InsertBooksStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#WIKI-1")
    @Test
    public void valid() {
        anna.is_the_home_page();
        anna.inserts("Titlu","AA,BB","Edit",123,"CC,DD");
        anna.should_see_message("{\"titlu\":\"Titlu\",\"editura\":\"Edit\",\"anAparitie\":123,\"autori\":[\"AA\",\"BB\"],\"cuvCheie\":[\"CC\",\"DD\"]}");

    }

    @Test
    public void nonValid() {
        anna.is_the_home_page();
        anna.inserts("","AA,BB","Edit",123,"CC,DD");
        anna.should_see_message("{\"issue\":[{\"error\":\"Text is missing\"}]}");
    }

} 