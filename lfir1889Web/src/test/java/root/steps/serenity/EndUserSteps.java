package root.steps.serenity;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import root.pages.LibraryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasToString;

public class EndUserSteps {

    LibraryPage libraryPage;

    @Step
    public void entersTitlu(String titlu)
    {
        libraryPage.enterTitlu( titlu );
    }

    @Step
    public void entersAutori(String autori)
    {
        libraryPage.enterAutori( autori );
    }
    @Step
    public void entersEditura(String editurs)
    {
        libraryPage.enterEditura( editurs );
    }
    @Step
    public void entersAnAparitie(int anAparitie)
    {
        libraryPage.enterAnAparitie( anAparitie );
    }
    @Step
    public void entersCuvCheie(String cuvCheie)
    {
        libraryPage.enterCuvCheie( cuvCheie );
    }

    @Step
    public void addBook()
    {
        libraryPage.addBook();
    }

    @Step
    public void should_see_message(String message) {
        String actualMessage = libraryPage.getResult();
        assertThat(actualMessage, hasToString( message ));
    }

    @Step
    public void is_the_home_page() {
        libraryPage.open();
    }

    @Step
    public void inserts(String titlu,String autori, String editura, int anAparitie, String cuvCheie) {
        entersTitlu(titlu);entersAutori(autori);entersEditura(editura);entersAnAparitie(anAparitie);entersCuvCheie(cuvCheie);
        addBook();
    }
}