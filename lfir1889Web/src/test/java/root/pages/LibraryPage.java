package root.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("file:///E:/Facultate/AN3%20SEM%20II/VVSS/LAB%201/GIT/lfir1889/keep/src/library.html")
public class LibraryPage extends PageObject {

    @FindBy(name="titlu")
    private WebElementFacade titluTB;
    @FindBy(name="autori")
    private WebElementFacade autoriTB;
    @FindBy(name="editura")
    private WebElementFacade edituraTB;
    @FindBy(name="anAparitie")
    private WebElementFacade anAparitieTB;
    @FindBy(name="cuvCheie")
    private WebElementFacade cuvCheieTB;

    @FindBy(name="addBookButton")
    private WebElementFacade addBookButton;

    public void enterTitlu(String titlu)
    {
        titluTB.type( titlu );
    }
    public void enterAutori(String autori)
    {
        autoriTB.type( autori );
    }
    public void enterEditura(String editura)
    {
        edituraTB.type( editura );
    }
    public void enterAnAparitie(int anAparitie)
    {
        anAparitieTB.type( String.valueOf( anAparitie ) );
    }
    public void enterCuvCheie(String cuvCheie)
    {
        cuvCheieTB.type( cuvCheie );
    }

    public void addBook()
    {
        addBookButton.click();
    }

    public String getResult() {
        WebElementFacade notification = find(By.name("message"));
        String result = notification.getText();
        return notification.getText();
    }
}