package lfir1889MV.view;


import lfir1889MV.control.ControllerBiblioteca;
import lfir1889MV.model.Carte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Consola {

	private BufferedReader console;
	ControllerBiblioteca controllerBiblioteca;
	
	public Consola(ControllerBiblioteca controllerBiblioteca){
		this.controllerBiblioteca=controllerBiblioteca;
	}
	
	public void executa() throws IOException {
		
		console = new BufferedReader(new InputStreamReader(System.in));
		
		int opt = -1;
		while(opt!=0){
			
			switch(opt){
				case 1:
					adauga();
					break;
				case 2:
					cautaCartiDupaAutor();
					break;
				case 3:
					afiseazaCartiOrdonateDinAnul();
					break;
				case 4:
					afiseazaToateCartile();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti un nr:");
				line=console.readLine();
			}while(!line.matches("[0-4]"));
			opt=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.print("\n\n");
		System.out.println("Evidenta cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     4. Afisarea toturor cartilor");
		System.out.println("     0. Exit");
	}
	
	public void adauga(){
		Carte carte = new Carte();
		try{
			System.out.print("\n");
			
			System.out.println("Titlu:");
			carte.setTitlu(console.readLine());
			
			String line;

			do{
				System.out.println("Nr. de autori:");
				line=console.readLine();
			}while(!line.matches("[1-9]"));	// de modificar in functie de cerinte

			int nrAutori= Integer.parseInt(line);

			for(int i=1;i<=nrAutori;i++)
			{
				System.out.println("Autor "+i+": ");
				carte.adaugaAutor(console.readLine());
			}

			System.out.println(	"Editura: " );
			carte.setEditura( console.readLine() );

			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}while(!line.matches("[1-9][0-9]*"));
			carte.setAnAparitie(Integer.parseInt( line ));

			do{
				System.out.println("Nr. de cuvinte cheie:");
				line=console.readLine();
			}while(!line.matches("[1-9]"));	// de modificar in functie de cerinte

			int nrCuvinteCheie= Integer.parseInt(line);

			for(int i=1;i<=nrCuvinteCheie;i++){
				System.out.println("Cuvant cheie "+i+": ");
				carte.adaugaCuvantCheie(console.readLine());
			}
			
			controllerBiblioteca.adaugaCarte(carte);
			
		}catch(Exception e){
			System.out.println( "Eroare la validare: " + e.getMessage() );
		}
	}
	
	public void afiseazaToateCartile(){
		System.out.print("\n");
		try {
			for(Carte carte: controllerBiblioteca.getCarti())
			{
				System.out.println( carte.toConsole( ) );
				System.out.print('\n' );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cautaCartiDupaAutor()
	{
		try {
			System.out.print("\n\n");
			System.out.println("Autor:");
			String autor = console.readLine();
			List<Carte> carti = controllerBiblioteca.getCartiByAutor( autor );

			carti.forEach( (carte) -> System.out.println(carte.toConsole()) );

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afiseazaCartiOrdonateDinAnul(){
		System.out.print("\n\n");
		try{
			String line;
			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}while(!line.matches("[1-9][0-9]{0,3}"));

			int anAparitie = Integer.parseInt( line );

			controllerBiblioteca.getCartiOrdonateDinAnul( anAparitie ).forEach( (carte) -> System.out.println(carte.toConsole()) );

		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
