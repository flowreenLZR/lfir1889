package lfir1889MV.model;


import java.util.ArrayList;
import java.util.List;

public class Carte {
	
	private String titlu;
	private List<String> autori;
	private int anAparitie;
	private List<String> cuvinteCheie;
	private String editura;
	
	public Carte(){
		titlu = "";
		autori = new ArrayList<String>();
		anAparitie = -1;
		cuvinteCheie = new ArrayList<String>();
		editura = "";
	}

	public String getEditura( )
	{
		return editura;
	}

	public void setEditura( String editura )
	{
		this.editura = editura;
	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public List<String> getAutori() {
		return autori;
	}

	public void setAutori(List<String> autor) {
		this.autori = autor;
	}

	public int getAnAparitie() {
		return anAparitie;
	}

	public void setAnAparitie(int anAparitie) {
		this.anAparitie = anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) {
		this.cuvinteCheie = cuvinteCheie;
	}

	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaAutor(String autor){
		autori.add(autor);
	}

	@Override
	public String toString(){
		String autor = "";
		String cuvCheie = "";
		
		for(int i=0;i<autori.size();i++){
			if(i==autori.size()-1)
				autor+=autori.get(i);
			else
				autor+=autori.get(i)+",";
		}
		
		for(int i=0;i<cuvinteCheie.size();i++){
			if(i==cuvinteCheie.size()-1)
				cuvCheie+=cuvinteCheie.get(i);
			else
				cuvCheie+=cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+autor+";"+anAparitie+";"+cuvCheie+";"+editura;
	}
	
	public static Carte getCarteFromString(String carte){
		Carte c = new Carte();
		String []atr = carte.split(";");
		String []autori = atr[1].split(",");
		String []cuvCheie = atr[3].split(",");
		
		c.titlu=atr[0];
		for(String s:autori){
			c.adaugaAutor(s);
		}
		c.anAparitie = Integer.parseInt(atr[2]);
		for(String s:cuvCheie){
			c.adaugaCuvantCheie(s);
		}
		c.editura = atr[4];
		
		return c;
	}

	public boolean hasAuthor( String autor )
	{
		for(String name : autori)
		{
			if(name.contains( autor )) return true;
		}
		return false;
	}

	public String toConsole( )
	{
		StringBuilder consoleCarte = new StringBuilder(  );

		consoleCarte.append( this.titlu );
		consoleCarte.append( '\n' );

		for (String autor : autori) consoleCarte.append( autor );
		consoleCarte.append( '\n' );

		consoleCarte.append( editura );
		consoleCarte.append( '\n' );

		consoleCarte.append( anAparitie );
		consoleCarte.append( '\n' );

		for(String cuvantCheie : cuvinteCheie) consoleCarte.append( cuvantCheie );

		return consoleCarte.toString();
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o ) return true;
		if ( o == null || getClass( ) != o.getClass( ) ) return false;

		Carte carte = ( Carte ) o;

		if ( anAparitie != carte.anAparitie ) return false;
		if ( titlu != null ? ! titlu.equals( carte.titlu ) : carte.titlu != null ) return false;
		if ( editura != null ? ! editura.equals( carte.editura ) : carte.editura != null) return false;
		if ( autori.size() != carte.getAutori().size() ) return false;
		if ( cuvinteCheie.size() != carte.getCuvinteCheie().size() ) return false;

		List<String> carteAutori = carte.getAutori();
		for ( String autor : autori)
		{
			if ( carteAutori.contains( autor ) )
				carteAutori.remove( autor );
			else
				return false;
		}

		List<String> carteKeyWords = carte.getCuvinteCheie();
		for ( String keyWord : cuvinteCheie)
		{
			if ( carteKeyWords.contains( keyWord ) )
				carteKeyWords.remove( keyWord );
			else
				return false;
		}
		return true;
	}


}
