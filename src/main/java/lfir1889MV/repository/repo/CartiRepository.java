package lfir1889MV.repository.repo;


import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepository implements CartiRepositoryInterface
{
	private String file = "cartiBD.txt";
	
	public CartiRepository(){ }

	public void adaugaCarte(Carte c)
	{
		BufferedWriter bufferedWriter = null;

		try {
			bufferedWriter = new BufferedWriter(new FileWriter(file,true));
			bufferedWriter.write(c.toString());
			bufferedWriter.newLine();

			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Carte> getCarti()
	{
		List<Carte> carti = new ArrayList<Carte>();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			String line = null;

			while((line=bufferedReader.readLine())!=null)
			{
				carti.add(Carte.getCarteFromString(line));
			}
			
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return carti;
	}

	public void modificaCarte(Carte nou, Carte vechi) {
		// TODO Auto-generated method stub
		
	}

	public void stergeCarte(Carte c) {
		// TODO Auto-generated method stub
		
	}

	public Carte getCarte(String titlu)
	{
		List<Carte> carti = getCarti();

		for(Carte carte : carti)
		{
			if(carte.getTitlu().equals( titlu )) return carte;
		}
		return null;
	}
}
