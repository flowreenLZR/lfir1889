package lfir1889MV.repository.repoMock;


import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepositoryMock implements CartiRepositoryInterface {

	private List<Carte> carti;
	
	public CartiRepositoryMock(){
		carti = new ArrayList<Carte>();
		
		/*carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
		carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
		carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
		carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
		carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
		carti.add(Carte.getCarteFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));*/

	}

	public void adaugaCarte(Carte c) {
		carti.add(c);
	}

	public Carte getCarte(String titlu)
	{
		List<Carte> carti = getCarti();

		for(Carte carte: carti)
		{
			if (carte.getTitlu().equals( titlu ))
			{
				return carte;
			}
		}

		return null;
	}

	public List<Carte> getCarti() {
		return carti;
	}

	public void modificaCarte(Carte nou, Carte vechi)
	{
		// TODO Auto-generated method stub
	}

	public void stergeCarte(Carte c)
	{
		// TODO Auto-generated method stub
	}


}
