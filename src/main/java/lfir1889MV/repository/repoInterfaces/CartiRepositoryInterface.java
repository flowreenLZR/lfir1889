package lfir1889MV.repository.repoInterfaces;


import lfir1889MV.model.Carte;

import java.util.List;

public interface CartiRepositoryInterface {
	void adaugaCarte(Carte carte);
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte carte);
	Carte getCarte(String titlu);
	List<Carte> getCarti();
}
