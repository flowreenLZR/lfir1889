package lfir1889MV.control;


import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.util.Validator;

import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ControllerBiblioteca {

	private CartiRepositoryInterface carteRepository;
	
	public ControllerBiblioteca(CartiRepositoryInterface carteRepository){
		this.carteRepository = carteRepository;
	}
	
	public void adaugaCarte(Carte carte) throws Exception{
		Validator.valideazaCarte(carte);
		carteRepository.adaugaCarte(carte);
	}
	
	public List<Carte> getCartiByAutor(String autor) throws Exception
	{
		List<Carte> carti = carteRepository.getCarti();
		List<Carte> cartiCautate = new ArrayList< Carte >(  );

		for(Carte carte : carti)
		{
			if ( carte.hasAuthor(autor))
				cartiCautate.add( carte );
		}

		return cartiCautate;
	}

	public List<Carte> getCarti(){return carteRepository.getCarti();}
	
	public List<Carte> getCartiOrdonateDinAnul(int anAparitie) throws Exception
	{
		if(anAparitie <= 0 || anAparitie > Year.now( ).getValue()) throw new Exception( "" );

		List<Carte> carti = carteRepository.getCarti();
		List<Carte> cartiCautate = new ArrayList< Carte >(  );

		for(Carte carte : carti)
		{
			if(carte.getAnAparitie() == anAparitie) cartiCautate.add( carte );
		}

		cartiCautate.sort( (carte1, carte2) ->
		{
			if (carte1.getTitlu().compareTo( carte2.getTitlu()) == 0 )
			{
				List<String> carte1Autori = carte1.getAutori( );
				List<String> carte2Autori = carte2.getAutori( );

				carte1Autori.sort( Comparator.comparing( s -> s ) );
				carte2Autori.sort( Comparator.comparing( s -> s ) );

				String stringAutori1 = carte1Autori.stream().reduce( "",( autor1, autor2 ) -> autor1 +autor2 );
				String stringAutori2 = carte2Autori.stream().reduce( "",( autor1, autor2 ) -> autor1 +autor2 );

				return stringAutori1.compareTo( stringAutori2 );
			}
			return carte1.getTitlu().compareTo( carte2.getTitlu() );
		});

		return cartiCautate;
	}
	
	
}
