package lfir1889MV.util;

import lfir1889MV.model.Carte;

import java.time.Year;
import java.util.Calendar;

public class Validator
{
	public static void valideazaCarte(Carte carte)throws Exception
	{
		if(carte.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(carte.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(! isWord(carte.getTitlu()))
			throw new Exception("Titlu invalid!");

		for(String s:carte.getAutori()){
			if(! isWord(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:carte.getCuvinteCheie()){
			if(! isWord(s))
				throw new Exception("Cuvant cheie invalid!");
		}

		if(! isWord(carte.getEditura()))
			throw new Exception("Editura invalida!");

		if(!(carte.getAnAparitie() <= Year.now().getValue() && carte.getAnAparitie() > 0 ))
			throw new Exception( "Anul de aparitie nu poate fi mai mare decat anul curent." );
	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}
	
	public static boolean isWord( String s){
		String []t = s.split(" ");
		for(String word : t)
		{
			if ( !word.matches( "[a-zA-Z]+" ))
				return false;
		}
		return true;
	}
	
}
