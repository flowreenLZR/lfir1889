package lfir1889MV.begin;

import lfir1889MV.control.ControllerBiblioteca;
import lfir1889MV.repository.repo.CartiRepository;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;
import lfir1889MV.view.Consola;

import java.io.IOException;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		CartiRepositoryInterface repositoryCarti = new CartiRepository();
		ControllerBiblioteca controllerBiblioteca = new ControllerBiblioteca(repositoryCarti);
		Consola consola = new Consola(controllerBiblioteca);
		try {
			consola.executa();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	CartiRepoInterface cr = new CartiRepo();
//	BibliotecaCtrl bc = new BibliotecaCtrl(cr);
//	
//	Carte c = new Carte();
//	bc = new BibliotecaCtrl(cr);
//	c = new Carte();
//	
//	List<String> autori = new ArrayList<String>();
//	autori.add("Mateiu Caragiale");
//	
//	List<String> cuvinteCheie = new ArrayList<String>();
//	cuvinteCheie.add("mateiu");
//	cuvinteCheie.add("crailor");
//	
//	c.setTitlu("Intampinarea crailor");
//	c.setAutori(autori);
//	c.setAnAparitie("1948");
//	c.setEditura("Litera");
//	c.setCuvinteCheie(cuvinteCheie);
//	
//	
//	try {
//		for(Carte ca:bc.getCartiOrdonateDinAnul("1948"))
//			System.out.println(ca);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//
//	
////	try {
////		bc.adaugaCarte(c);
////	} catch (Exception e) {
////		// TODO Auto-generated catch block
////		e.printStackTrace();
////	}
	
}
