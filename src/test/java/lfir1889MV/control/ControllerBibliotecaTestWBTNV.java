package lfir1889MV.control;

import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ControllerBibliotecaTestWBTNV
{
    private ControllerBiblioteca controllerBiblioteca;
    private CartiRepositoryInterface cartiRepository;
    private Carte carte;

    @org.junit.Before
    public void setUp( ) throws Exception
    {
        System.out.println("Begam setup" );
        cartiRepository = new CartiRepositoryMock();
        controllerBiblioteca = new ControllerBiblioteca( cartiRepository );

        carte = new Carte();
        carte.setTitlu( "Povestiri din Kolima" );
        carte.setAutori( new ArrayList<>( Arrays.asList( "Varlam Salamov" )));
        carte.setEditura( "Polirom" );
        carte.setAnAparitie( 2015 );
        carte.setCuvinteCheie( Arrays.asList( "Siberia", "lagar", "humanity" ) );

        System.out.println("finished setup" );
    }

    @org.junit.After
    public void tearDown( ) throws Exception
    {
    }


    @org.junit.Test
    public void getCartiByAuor_WBT_nonvalid() throws Exception
    {
        getCartiByAutor_TC1();
        getCartiByAutor_TC2();
    }

    public void getCartiByAutor_TC1( ) throws Exception
    {
        List<Carte> result = controllerBiblioteca.getCartiByAutor( "random" );
        assertTrue( result.size() == 0 );
    }

    public void getCartiByAutor_TC2( ) throws Exception
    {
        cartiRepository.adaugaCarte( carte );
        List<Carte> result = controllerBiblioteca.getCartiByAutor( "random" );
        assertTrue( result.size() == 0 );
    }
}