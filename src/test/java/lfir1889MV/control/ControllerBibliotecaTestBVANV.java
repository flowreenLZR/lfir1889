package lfir1889MV.control;

import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;

public class ControllerBibliotecaTestBVANV
{
    private ControllerBiblioteca controllerBiblioteca;
    private CartiRepositoryInterface cartiRepository;
    private Carte carte;

    @org.junit.Before
    public void setUp( ) throws Exception
    {
        System.out.println("Begam setup" );
        cartiRepository = new CartiRepositoryMock();
        controllerBiblioteca = new ControllerBiblioteca( cartiRepository );

        carte = new Carte();
        carte.setTitlu( "Povestiri din Kolima" );
        carte.setAutori( new ArrayList<>( Arrays.asList( "Varlam Salamov" )));
        carte.setEditura( "Polirom" );
        carte.setAnAparitie( 2015 );
        carte.setCuvinteCheie( Arrays.asList( "Siberia", "lagar", "humanity" ) );

        System.out.println("finished setup" );
    }

    @org.junit.After
    public void tearDown( ) throws Exception
    {
    }



    @org.junit.Test()
    public void adaugaCarte_BBT_BVA_NonValid( ) throws Exception
    {
        carte.setAnAparitie( Year.now( ).getValue() + 1 );

        try
        {
            controllerBiblioteca.adaugaCarte( carte );
        }
        catch ( Exception e )
        {
            assertArrayEquals( "Anul de aparitie nu poate fi mai mare decat anul curent.".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse( "The execution should have not reached this point!!!",true );
    }
}