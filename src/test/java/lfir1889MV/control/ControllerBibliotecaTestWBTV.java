package lfir1889MV.control;

import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ControllerBibliotecaTestWBTV
{
    private ControllerBiblioteca controllerBiblioteca;
    private CartiRepositoryInterface cartiRepository;
    private Carte carte;

    @org.junit.Before
    public void setUp( ) throws Exception
    {
        System.out.println("Begam setup" );
        cartiRepository = new CartiRepositoryMock();
        controllerBiblioteca = new ControllerBiblioteca( cartiRepository );

        carte = new Carte();
        carte.setTitlu( "Povestiri din Kolima" );
        carte.setAutori( new ArrayList<>( Arrays.asList( "Varlam Salamov" )));
        carte.setEditura( "Polirom" );
        carte.setAnAparitie( 2015 );
        carte.setCuvinteCheie( Arrays.asList( "Siberia", "lagar", "humanity" ) );

        System.out.println("finished setup" );
    }

    @org.junit.After
    public void tearDown( ) throws Exception
    {
    }

    @org.junit.Test
    public void getCartiByAutor_TC3( ) throws Exception
    {
        Carte secondBook = new Carte(); secondBook.setTitlu( "IvanIlici" );secondBook.setAutori( Arrays.asList("Tolstoi") );secondBook.setAnAparitie( 1870 );secondBook.setEditura( "BPT" );secondBook.setCuvinteCheie( Arrays.asList("rusia") );
        cartiRepository.adaugaCarte( carte );
        cartiRepository.adaugaCarte( secondBook );
        List<Carte> result = controllerBiblioteca.getCartiByAutor( "Tol" );
        assertTrue( result.size() == 1 );
    }
}