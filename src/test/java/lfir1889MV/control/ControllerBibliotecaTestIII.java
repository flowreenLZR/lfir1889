package lfir1889MV.control;

import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoInterfaces.CartiRepositoryInterface;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ControllerBibliotecaTestIII
{
    private ControllerBiblioteca controllerBiblioteca;
    private CartiRepositoryInterface cartiRepository;
    private Carte carte;

    @org.junit.Before
    public void setUp( ) throws Exception
    {
        cartiRepository = new CartiRepositoryMock();
        controllerBiblioteca = new ControllerBiblioteca( cartiRepository );

        addABookWithYear( 1990,"Povestiri din Kolima", new ArrayList<>( Arrays.asList( "Varlam Salamov" )));
        addABookWithYear( 1980,"Povestiri din Kolima", new ArrayList<>( Arrays.asList( "Varlam Salamov" )));
        addABookWithYear( 1980,"Qovestiri din Kolima", new ArrayList<>( Arrays.asList( "Varlam Salamov","Rarlam Salamov" )));
        addABookWithYear( 1980,"Qovestiri din Kolima", new ArrayList<>( Arrays.asList( "Varlam Salamov","Sarlam Salamov" )));

    }

    public void addABookWithYear(int year, String title, List<String> authors)
    {
        carte = new Carte();
        carte.setTitlu( title );
        carte.setAutori( authors);
        carte.setEditura( "Polirom" );
        carte.setAnAparitie( year );
        carte.setCuvinteCheie( Arrays.asList( "Siberia", "lagar", "humanity" ) );

        try
        {
            controllerBiblioteca.adaugaCarte( carte );
        }
        catch ( Exception e )
        {
            e.printStackTrace( );
        }
    }

    @org.junit.After
    public void tearDown( ) throws Exception
    {
    }

    @org.junit.Test()
    public void test_reqIII_Valid( )
    {
        List<Carte> carti = null;
        try
        {
            carti = controllerBiblioteca.getCartiOrdonateDinAnul( 1980);

            assertTrue( carti.size() == 3 );
        }
        catch ( Exception e )
        {
            assertFalse( "Shouldn't be here",true );
        }

    }

    @org.junit.Test()
    public void test_reqIII_NonValid( )
    {
        List<Carte> carti = null;
        try
        {
            carti = controllerBiblioteca.getCartiOrdonateDinAnul( -2);

            assertFalse( "Shouldn't be here",true );
        }
        catch ( Exception e )
        {
            assertTrue( true );
        }

    }
}
