package lfir1889MV.control;

import lfir1889MV.model.Carte;
import lfir1889MV.repository.repoMock.CartiRepositoryMock;
import lfir1889MV.view.Consola;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class IntegrationTests
{
    ControllerBibliotecaTestBVANV cbtBVANV;
    ControllerBibliotecaTestBVAV cbtBVAV;
    ControllerBibliotecaTestBVAVE cbtBVAVE;
    ControllerBibliotecaTestECPV cbtECPV;
    ControllerBibliotecaTestECPNV cbtECPNV;
    ControllerBibliotecaTestWBTV cbtWBTV;
    ControllerBibliotecaTestWBTNV cbtWBTNV;

    ControllerBibliotecaTestIII cbtIII;

    CartiRepositoryMock cartiRepositoryMock;
    ControllerBiblioteca controllerBiblioteca;
    Consola consola;

    @org.junit.Before
    public void setUp( ) throws Exception
    {
        cbtBVANV = new ControllerBibliotecaTestBVANV();
        cbtBVAV = new ControllerBibliotecaTestBVAV();
        cbtBVAVE = new ControllerBibliotecaTestBVAVE();
        cbtECPV = new ControllerBibliotecaTestECPV();
        cbtECPNV = new ControllerBibliotecaTestECPNV();
        cbtWBTV = new ControllerBibliotecaTestWBTV();
        cbtWBTNV = new ControllerBibliotecaTestWBTNV();

        cbtIII = new ControllerBibliotecaTestIII();
        cartiRepositoryMock = new CartiRepositoryMock();
        controllerBiblioteca = new ControllerBiblioteca( cartiRepositoryMock );
        consola = new Consola( controllerBiblioteca );
    }

    @org.junit.After
    public void tearDown( ) throws Exception
    { }

    @org.junit.Test()
    public void test()
    {
        int_bb_I();
        int_bb_II();
        int_bb_III();
        int_bb_P();
    }

    public void int_bb_I( )
    {
        try
        {
            cbtBVANV.setUp();
            cbtBVANV.adaugaCarte_BBT_BVA_NonValid();
            cbtBVANV.tearDown();

            cbtBVAV.setUp();
            cbtBVAV.adaugaCarte_BBT_BVA_Valid();
            cbtBVAV.tearDown();

            cbtBVAVE.setUp();
            cbtBVAVE.adaugaCarte_BBT_BVA_Valid_Edge();
            cbtBVAVE.tearDown();

            cbtECPNV.setUp();
            cbtECPNV.adaugaCarte_BBT_ECP_NonValid();
            cbtECPNV.tearDown();

            cbtECPV.setUp();
            cbtECPV.adaugaCarte_BBT_ECP_Valid();
            cbtECPV.tearDown();

            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Shouldn't reached this point",true );
        }

    }

    public void int_bb_II( )
    {
        try
        {
            cbtWBTV.setUp();
            cbtWBTV.getCartiByAutor_TC3();
            cbtWBTV.tearDown();

            cbtWBTNV.setUp();
            cbtWBTNV.getCartiByAuor_WBT_nonvalid();
            cbtWBTNV.tearDown();

            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Shouldn't reached this point",true );
        }

    }

    public void int_bb_III( )
    {
        try
        {
            cbtIII.setUp();
            cbtIII.test_reqIII_Valid();
            cbtIII.tearDown();

            cbtIII.setUp();
            cbtIII.test_reqIII_NonValid();
            cbtIII.tearDown();

            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Shouldn't reached this point",true );
        }

    }

    public void int_bb_P()
    {
        testI();
        testII();
        testIII();
    }

    public void testIII()
    {

        try
        {
            System.setIn( new FileInputStream( "testIII.txt" ) );
            System.setOut( new PrintStream( "testIOut.txt" ) );
            consola.executa();

            BufferedReader reader = new BufferedReader( new FileReader( "testIOut.txt" ) );
            StringBuilder contentBuilder = new StringBuilder(  );
            String content = "";
            reader.lines().forEach( line -> contentBuilder.append( line ));

            content = contentBuilder.toString();
            assertTrue( content.contains( "TestaAutAuttEdit123cuvcuvvTestbAutaAuttEdit123cuvcuvvTestbAutbAuttEdit123cuvcuvv" ));
        }
        catch ( IOException e )
        {
            assertFalse(true);
        }
    }

    public void testII()
    {
        try
        {
            System.setIn( new FileInputStream( "testII.txt" ) );
            System.setOut( new PrintStream( "testIOut.txt" ) );
            consola.executa();
            BufferedReader reader = new BufferedReader( new FileReader( "testIOut.txt" ) );
            StringBuilder contentBuilder = new StringBuilder(  );
            String content = "";
            reader.lines().forEach( line -> contentBuilder.append( line ));

            content = contentBuilder.toString();
            assertTrue( content.contains( "TestAutAuttEdit123cuvcuvv" ));
        }
        catch ( IOException e )
        {
            assertFalse( true );
        }

    }

    public void testI()
    {
        try
        {
            System.setIn( new FileInputStream( "testI.txt" ) );
            System.setOut( new PrintStream( "testIOut.txt" ) );
            consola.executa();
            Carte carte = cartiRepositoryMock.getCarte( "Test" );
            Carte expected = new Carte();
            expected.setTitlu( "Test" );
            expected.setEditura( "Edit" );
            expected.setAnAparitie( 123 );
            expected.setAutori( new ArrayList<>( Arrays.asList( "Aut","Autt" ) ) );
            expected.setCuvinteCheie( new ArrayList<>( Arrays.asList( "cuv","cuvv" ) ) );
            assertTrue( carte.equals( expected ));
        }
        catch ( IOException e )
        {
            assertFalse( true );
        }
    }


}
